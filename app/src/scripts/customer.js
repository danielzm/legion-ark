/* jshint -W069 */
/* global require, module, $, console, window*/

"use strict";


require('bootstrap-browserify');
require('./theme-libs/common-scripts');
require('header');

var Handlebars = require('./external_libs/handlebars/handlebars.min.js'),
  FBCustomers = require('./ark/fb-models/fb-customers'),
  _ = require('./external_libs/underscore/underscore-min.js'),
  List = require('./external_libs/list.js/dist/list.min.js'),
  arkUtils = require('./ark/ark-utils');

var dataCustomers = new FBCustomers(),
  params = arkUtils.getParamsObjectFromURL(window.location.href),
  actualCustomerRef,
  selectors = {
    updateAlertSuccess: '[role="alert"].update-alert.alert-success',
    customerForm: '.customer-edit-page form',
    saveBtn: '.customer-edit-page form button.save'
  };

var getFormDataObject = function () {
  var formArray = $(selectors.customerForm)
    .serializeArray(),
    formData = {};

  formArray = _.each(_.union(formArray), function (value, key) {
    formData[value.name] = value.value;
    return;
  });

  return formData;
};

var updateActualCustomer = function () {
  if (!actualCustomerRef) {
    return;
  }

  actualCustomerRef.update(getFormDataObject(), function (error) {
    if (error) {
      throw new Error("Data could not be saved." + error);
    } else {
      $(selectors.updateAlertSuccess)
        .show();
    }
  });

  return;
};

var setListeners = function () {
  $(selectors.saveBtn)
    .on('click', function (event) {
      event.preventDefault();
      updateActualCustomer();
    });
};

var customersCount = function () {
  var customersCount;
  dataCustomers.connection.once("value", function (snap) {
    customersCount = _.keys(snap.val())
      .length;
  });
  return customersCount;
};

var getActualCustomerData = function (callback) {
  actualCustomerRef.once('value', function (snap) {
    callback(snap.val());
  });
  return;
};

var writeDataToForm = function (customerData) {
  var $form = $(selectors.customerForm);

  _.each(customerData, function (value, key) {
    var $input = $form.find('[name="' + key + '"]');
    $input.val(value);
  });
};

var loadActualCustomer = function () {
  getActualCustomerData(function (customerData) {
    writeDataToForm(customerData);
    return;
  });
  return;
};

if (!params.uid) {
  throw new Error('Error: no user uid parameter!');
} else {
  setListeners();

  if (params.uid !== 'new') {
    actualCustomerRef = dataCustomers.connection.child(params.uid);
    loadActualCustomer();
  } else {
    dataCustomers.getLastCustomer(function (lastCustomer) {
      var newId = 1;

      if (_.isObject(lastCustomer) && lastCustomer.id) {
        newId += lastCustomer.id;
      }

      var newCustomerData = {
        "id": newId,
        "date-added": Date()
      };

      actualCustomerRef = dataCustomers.connection.push(newCustomerData, function (error) {
        if (error) {
          throw new Error("Not able to initialize the customer." + error);
        } else {
          writeDataToForm(newCustomerData);
        }
      });
    });
  }
}