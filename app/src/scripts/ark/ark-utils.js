/* jshint -W069 */
/* global require, module, FileReader */

'use strict';

/* Ark Utils*/

module.exports = (function () {
  var $ = require('jquery'),
    _ = require('../external_libs/underscore/underscore-min.js');

  return {
    getParamsObjectFromURL: function (url) {
      var getQueryString = this.getQueryString(url),
        params = {};

      if (getQueryString) {
        params = this.getQueryParams(getQueryString);
      }

      return params;
    },

    getQueryString: function (url) {
      return url.split("?")[1];
    },

    getQueryParams: function (qs) {
      qs = qs.split("+")
        .join(" ");

      var params = {},
        tokens,
        re = /[?&]?([^=]+)=([^&]*)/g;

      while ((tokens = re.exec(qs))) {
        params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
      }

      return params;
    },

    getFormDataObject: function (selector) {
      var formArray = $(selector)
        .serializeArray(),
        formData = {};

      formArray = _.each(_.union(formArray), function (value, key) {
        formData[value.name] = value.value;
        return;
      });

      return formData;
    },

    csv2Object: function (file, callback) {
      var fileContent,
        lines,
        headers,
        ObjectResult = [],
        fileReader = new FileReader();

      var cleanStrings = function (strings) {
        var cleanStringsArr = [];
        _.each(strings, function (str, index) {
          cleanStringsArr[index] = str.replace(/\"/g, '');
        });
        return cleanStringsArr;
      };

      fileReader.onload = function (e) {
        fileContent = e.target.result;
        lines = cleanStrings(fileContent.split("\n"));
        headers = lines.shift()
          .split(",");

        _.each(lines, function (line) {
          var obj = {};
          var currentline = line.split(",");

          _.each(headers, function (column, index) {
            obj[column] = currentline[index];
          });

          ObjectResult.push(obj);
        });

        callback(ObjectResult);
      };

      fileReader.readAsText(file);
      return;
    }

  };
})();