'use strict';

/**/

module.exports = (function () {
  var mainDataConnectionURL = 'https://the-ark-dev2.firebaseio.com/';
  var attributes = {
    'firebase': {
      'main-url': mainDataConnectionURL,
      'customers-url': 'customers/',
      'company-url': {
        'legion': 'legion/'
      }
    }
  };

  var getMainPathForCompany = function (company) {
    return attributes.firebase['main-url'] + attributes.firebase['company-url'][company];
  };

  return function () {
    this.getMainPathForCompany = getMainPathForCompany;
    this.attributes = attributes;
    return;
  };

})();