'use strict';


/* firebase connection class */
var ArkConfig = require('../ark-config'),
  Firebase = require('firebase');

module.exports = (function () {
  // Private static attributes.
  var numOfRegistries = 0;

  // Private static method.
  var startConnection = function (url) {
    return new Firebase(url);
  };

  // Return the constructor.
  return function (company) {
    // Private attributes
    var Config = new ArkConfig(),
      connectionUrl = Config.attributes.firebase['main-url'];

    // public
    this.company = company || '';
    if (company) {
      connectionUrl = Config.getMainPathForCompany(company);
    }

    this.connection = startConnection(connectionUrl);
    return;
  };
})();