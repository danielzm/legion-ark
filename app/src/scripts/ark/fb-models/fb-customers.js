'use strict';

/* global module, require, $, console */

/* Firebase customers connection */

var DataStore = require('./fb-connection.js'),
  ArkConfig = require('../ark-config.js'),
  _ = require('../../external_libs/underscore/underscore-min.js');

module.exports = (function () {
  // set the customer from config
  var dataStore = new DataStore('legion'),
    Config = new ArkConfig();

  var getCustomersChild = function (dataStore) {
    return dataStore.connection.child(Config.attributes.firebase['customers-url']);
  };

  var _getLastCustomer = function (callback) {
    this.connection
      .orderByChild('id')
      .limitToLast(1)
      .once('value', function (snap) {
        var lastCustomerObject = snap.val() || null,
        lastCustomer;

        if (_.isObject(lastCustomerObject) && !_.isEmpty(lastCustomerObject)) {
          lastCustomer = lastCustomerObject[_.keys(lastCustomerObject)];
        }
        callback(lastCustomer);
        return;
    });
  };

  var _createNewCustomer = function(customerData, callback) {
    var actualCustomerRef = {},
    that = this;
    this.getLastCustomer(function (lastCustomer) {
      var newId = 1;
      if (_.isObject(lastCustomer) && lastCustomer.id) {
        newId += lastCustomer.id;
      }

      _.extend(customerData, {
        "id": newId,
        "date-added": Date()
      });

      var actualCustomerRef = that.connection.push(customerData, function (error) {
        if (error) {
          throw new Error("Not able to initialize the customer." + error);
        } else {
          callback(customerData);
        }
      });
    });
  };

  var _importCustomers = function(customersJSON, callback){
    var that = this,
      customerData = customersJSON.shift();

    var newCustomerData = {
      "comments": customerData.Comentarios || "",
      "email": customerData.Correo || "",
      "name": customerData.Nombre || "",
      "origin": customerData.Origin || "",
      "phone": customerData.Telefono || ""
    };

    that.createNewCustomer(newCustomerData, function(customerData){
      if (customersJSON.length > 0) {
        that.importCustomers(customersJSON);
        console.log('Customers: customer '+customerData.name+' <'+customerData.email+'> '+' imported.');
      } else {
        console.log('Customers: All customers imported.');
      }
    });
  };

  return function () {
    this.connection = getCustomersChild(dataStore);
    this.getLastCustomer = _getLastCustomer;
    this.importCustomers = _importCustomers;
    this.createNewCustomer = _createNewCustomer;
  };
})();
