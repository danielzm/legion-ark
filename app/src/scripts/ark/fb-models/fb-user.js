/* global require, console, module, $ */

'use strict';

/* Firebase customers connection */
var ArkDataStore = require('./fb-connection.js'),
  ArkConfig = require('../ark-config.js'),
  _ = require('../../external_libs/underscore/underscore-min.js');

module.exports = (function () {
  // set the customer from config
  var dataStore = new ArkDataStore('legion');

  var _login = function (email, password) {
    if (!email || email === "" || !password || password === "") {
      console.error('User Login: email and password parameters needed.');
    } else if (_isLogged()) {
      console.log('User already logged in, logout and try again.');
      return;
    }

    this.email = email;
    this.password = password;

    dataStore.connection.authWithPassword({
      email: email,
      password: password
    }, _loginResult);
  };

  var _isLogged = function () {
    if (!dataStore.connection.getAuth()) {
      return;
    }
    return true;
  };

  var _loginResult = function (error, authData) {
    if (error === null) {
      console.log("User ID: " + authData.uid + ", Provider: " + authData.provider);
    } else {
      console.log("Error authenticating user, try again.");
    }
  };

  var _logout = function () {
    dataStore.connection.unauth();
  };

  // Return the constructor.
  return function (params) {
    // Private attributes
    this.connection = dataStore;

    // public methods
    this.isLogged = _isLogged;
    this.login = _login;
    this.logout = _logout;

    if (params) {
      this.email = params.email || '';
      this.password = params.password || '';

      if (!_isLogged()) {
        _login(this.email, this.password);
      } else {
        console.log('Openned Session, please logout and try again.');
      }
    }
    return;
  };
})();