/* jshint -W069 */
/* global require, console, window, $*/

"use strict";

require('bootstrap-browserify');
require('./theme-libs/common-scripts');
require('./external_libs/jquery-backstretch/jquery.backstretch.min.js');

var _ = require('./external_libs/underscore/underscore-min.js'),
  arkUtils = require('./ark/ark-utils'),
  FBUser = require('./ark/fb-models/fb-user'),
  initalPage = "customers.html";

var userRef = new FBUser();

// detect the logout and redirect to login page
userRef.connection.connection.onAuth(function (authData) {
  if (authData) {
    window.location.assign(initalPage);
  }
});

$.backstretch("src/img/bg-login.jpg", {
  speed: 500
});

$("#login")
  .submit(function (event) {
    event.preventDefault();
    var loginFormData = arkUtils.getFormDataObject("#login");
    userRef.login(loginFormData.email, loginFormData.password);
  });