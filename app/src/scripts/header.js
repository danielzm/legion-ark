'use strict';
/* global require, $, console, window */

/* customers.html page */

var mainPageSection = "index.html",
  FBUser = require('./ark/fb-models/fb-user'),
  userRef = new FBUser();

// detect the logout and redirect to login page
userRef.connection.connection.onAuth(function(authData) {
  if (!authData) {
    window.location.assign(mainPageSection);
  }
});

$('ul.nav li a.logout').on('click', function(event) {
  event.preventDefault();
  userRef.logout();
});
