/* jshint -W069 */
/* global require, $, console*/

"use strict";

/* customers.html page */

require('bootstrap-browserify');
require('./theme-libs/common-scripts');
require('header');

var Handlebars = require('./external_libs/handlebars/handlebars.min.js'),
  FBCustomers = require('./ark/fb-models/fb-customers'),
  _ = require('./external_libs/underscore/underscore-min.js'),
  List = require('./external_libs/list.js/dist/list.min.js'),
  ArkUtils = require('./ark/ark-utils.js');

var dataCustomers = new FBCustomers(),
  tableRowTemplate = Handlebars.compile($('#customer-row')
    .html()),
  customerList,
  customersCount,
  selectors = {
    deleteBtn: '#client-table tbody .btn.delete',
    deleteAlertSuccess: '[role="alert"].delete-alert.alert-success',
    deleteModal: {
      itself: '#delete-modal .modal-dialog',
      customerId: '.delete-modal .id',
      customerName: '.delete-modal .name',
      cancelBtn: '.delete-modal button.cancel',
      deleteBtn: '.delete-modal button.delete'
    }
  };

var printCustomers = function (customers) {
  $('#client-table tbody')
    .html('');
  $('#client-table tbody')
    .append(tableRowTemplate({
      customers: customers
    }));

  customerList = new List('customers-table', {
    page: 500,
    valueNames: ['id', 'name', 'phone', 'email', 'comments']
  });
};

var setListeners = function () {
  $(selectors.deleteBtn)
    .on('click', function (event) {
      event.preventDefault();
      var uid = $(this)
        .data('uid'),
        customerData = dataCustomers.connection.child(uid);

      customerData.once('value', function (snap) {
        var $id = $(selectors.deleteModal.customerId),
          $name = $(selectors.deleteModal.customerName);

        $(selectors.deleteModal.itself)
          .data('uid', uid);
        $id.html('(' + snap.val()
          .id + ')');
        $name.html(snap.val()
          .name);
      });
    });

  $(selectors.deleteModal.deleteBtn)
    .on('click', function () {
      var $modal = $(selectors.deleteModal.itself),
        uid = $modal.data('uid'),
        customerId = $(selectors.deleteModal.customerId)
        .html(),
        customerName = $(selectors.deleteModal.customerName)
        .html(),
        customerDeleted = customerName || customerId,
        customerData = dataCustomers.connection.child(uid);

      customerData.remove(function (error) {
        if (error) {
          console.log('Error: imposible to delete customer: ', error);
        } else {
          $(selectors.deleteAlertSuccess)
            .show();
          $('#delete-modal')
            .modal('hide');
        }
      });
    });

  $('form#csv-file > a')
    .on('click', function (event) {
      event.preventDefault();
      $('form#csv-file input[type="file"]')
        .trigger('click');
    });

  $('form#csv-file input[type="file"]')
    .on('change', function (event) {
      event.preventDefault();
      var JSONResult = [],
        file = $('form#csv-file input[type="file"]')[0].files[0];

      ArkUtils.csv2Object(file, function (ObjectResult) {
        dataCustomers.importCustomers(ObjectResult);
      });
    });

};

dataCustomers.connection.orderByChild("id")
  .on("value", function (snapshot) {
    var customers = _.map(snapshot.val(), function (customerData, customerKey) {
      return _.extend(customerData, {
        uid: customerKey
      });
    });

    printCustomers(customers);
    setListeners();
    return;
  });