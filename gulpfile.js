var gulp = require('gulp'),
  connect = require('gulp-connect'),
  gutil = require('gulp-util'),
  less = require('gulp-less'),
  source = require('vinyl-source-stream'),
  watchify = require('watchify'),
  browserify = require('browserify'),
  jshint = require('gulp-jshint'),
  packageJSON = require('./package'),
  jshintConfig = packageJSON.jshintConfig,
  dest = "./app/build",
  src = './app/src',
  scripts = [src + '/scripts/*.js', src + '/scripts/ark/**/*.js', '!' + src + '/scripts/theme-libs/**/*.js'],
  styles = src + '/styles/less/**/*.less';

jshintConfig.lookup = false;

gulp.src('yo')
  .pipe(jshint(jshintConfig));

var mainConfig = {
  browserify: {
    // Enable source maps
    debug: true,
    // Additional file extentions to make optional
    extensions: ['.hbs'],
    // A separate bundle will be generated for each
    // bundle config in the list below
    bundleConfigs: [{
      entries: src + '/scripts/customer.js',
      dest: dest,
      outputName: 'customer.js'
    }, {
      entries: src + '/scripts/customers.js',
      dest: dest,
      outputName: 'customers.js'
    }, {
      entries: src + '/scripts/login.js',
      dest: dest,
      outputName: 'login.js'
    }]
  },
  markup: {
    src: src + "./app/*.html",
    dest: dest
  }
};

gulp.task('connect', function () {
  connect.server({
    root: 'app',
    livereload: true
  });
});

//  Browserify  //
gulp.task('setWatch', function () {
  global.isWatching = true;
});

gulp.task('browserify', function (callback) {

  var config = mainConfig.browserify,
    bundleQueue = config.bundleConfigs.length;

  var browserifyThis = function (bundleConfig) {

    var bundler = browserify({
      // Required watchify args
      cache: {},
      packageCache: {},
      fullPaths: true,
      // Specify the entry point of your app
      entries: bundleConfig.entries,
      // Add file extentions to make optional in your requires
      extensions: config.extensions,
      // Enable source maps!
      debug: config.debug,

      paths: ['./node_modules',
        './app/src/scripts',
        './app/src/scripts/ark',
        './app/src/scripts/libs',
        './app/src/scripts/fb-models'
      ]
    });

    var bundle = function () {
      return bundler
        .bundle()
        // Report compile errors
        .on('error', function (error) {
          console.log('Browserify Bundle Error: ' + error);
          this.emit('end');
        })
        // Use vinyl-source-stream to make the
        // stream gulp compatible. Specifiy the
        // desired output filename here.
        .pipe(source(bundleConfig.outputName))
        // Specify the output destination
        .pipe(gulp.dest(bundleConfig.dest))
        .on('end', reportFinished);
    };

    if (global.isWatching) {
      // Wrap with watchify and rebundle on changes
      bundler = watchify(bundler);
      // Rebundle on update
      bundler.on('update', bundle);
    }

    var reportFinished = function () {
      // Log when bundling completes
      console.log("Browserifying: " + bundleConfig.outputName)

      if (bundleQueue) {
        bundleQueue--;
        if (bundleQueue === 0) {
          // If queue is empty, tell gulp the task is complete.
          // https://github.com/gulpjs/gulp/blob/master/docs/API.md#accept-a-callback
          callback();
        }
      }
    };

    return bundle();
  };

  // Start bundling with Browserify for each bundleConfig specified
  config.bundleConfigs.forEach(browserifyThis);
});

gulp.task('less', function () {
  console.log('Compiling Less.');
  gulp.src(styles)
    .pipe(less())
    .pipe(gulp.dest(dest + '/css'))
    .pipe(connect.reload());
});

jshintConfig.lookup = false;

gulp.task('lint', function () {
  return gulp.src(scripts)
    .pipe(jshint(jshintConfig))
    .pipe(jshint.reporter('default'));
});

gulp.task('html', function () {
  gulp.src('./app/*.html')
    .pipe(connect.reload());
});

gulp.watch(['./app/*.html'], ['html']);
gulp.watch(scripts, ['lint']);
gulp.watch(styles, ['less']);

gulp.task('default', ['connect', 'setWatch', 'browserify', 'lint', 'less']);